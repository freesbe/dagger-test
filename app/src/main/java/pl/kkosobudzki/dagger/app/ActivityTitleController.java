package pl.kkosobudzki.dagger.app;

import android.app.Activity;

/**
 * @author Krzysztof Kosobudzki
 */
public class ActivityTitleController {
    private final Activity mActivity;

    public ActivityTitleController(Activity activity) {
        mActivity = activity;
    }

    public void setTitle(CharSequence title) {
        mActivity.setTitle(title);
    }
}
