package pl.kkosobudzki.dagger.app.module;

import android.content.Context;
import android.location.LocationManager;
import dagger.Module;
import dagger.Provides;
import pl.kkosobudzki.dagger.app.App;
import pl.kkosobudzki.dagger.app.ForApplication;

import javax.inject.Singleton;

/**
 * @author Krzysztof Kosobudzki
 */
@Module(library = true)
public class AndroidModule {
    private final App mApp;

    public AndroidModule(App app) {
        mApp = app;
    }

    @Provides
    @Singleton
    @ForApplication
    public Context provideApplicationContext() {
        return mApp;
    }

    @Provides
    @Singleton
    public LocationManager provideLocationManager() {
        return (LocationManager) mApp.getSystemService(Context.LOCATION_SERVICE);
    }
}
