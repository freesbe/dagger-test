package pl.kkosobudzki.dagger.app;

import android.app.Application;
import dagger.ObjectGraph;
import pl.kkosobudzki.dagger.app.module.AndroidModule;

import java.util.Arrays;
import java.util.List;

/**
 * @author Krzysztof Kosobudzki
 */
public class App extends Application {
    private ObjectGraph mObjectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        mObjectGraph = ObjectGraph.create(getModules().toArray());
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new AndroidModule(this));
    }

    ObjectGraph getObjectGraph() {
        return mObjectGraph;
    }
}
