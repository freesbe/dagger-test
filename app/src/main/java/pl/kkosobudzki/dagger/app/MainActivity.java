package pl.kkosobudzki.dagger.app;

import android.app.Activity;
import android.location.LocationManager;
import android.os.Bundle;

import javax.inject.Inject;

public class MainActivity extends Activity {
    @Inject LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }
}
