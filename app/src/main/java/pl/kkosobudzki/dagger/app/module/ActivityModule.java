package pl.kkosobudzki.dagger.app.module;

import android.content.Context;
import dagger.Module;
import dagger.Provides;
import pl.kkosobudzki.dagger.app.ActivityTitleController;
import pl.kkosobudzki.dagger.app.BaseActivity;
import pl.kkosobudzki.dagger.app.ForActivity;
import pl.kkosobudzki.dagger.app.MainActivity;

import javax.inject.Singleton;

/**
 * @author Krzysztof Kosobudzki
 */
@Module(
        injects = {
                MainActivity.class
        },
        addsTo = AndroidModule.class,
        library = true
)
public class ActivityModule {
    private final BaseActivity mActivity;

    public ActivityModule(BaseActivity activity) {
        mActivity = activity;
    }

    @Provides
    @Singleton
    @ForActivity
    public Context providesActivityContext() {
        return mActivity;
    }

    @Provides
    @Singleton
    public ActivityTitleController providesActivityTitleController() {
        return new ActivityTitleController(mActivity);
    }
}
