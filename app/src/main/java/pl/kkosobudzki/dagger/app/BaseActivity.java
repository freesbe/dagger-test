package pl.kkosobudzki.dagger.app;

import android.app.Activity;
import android.os.Bundle;
import dagger.ObjectGraph;
import pl.kkosobudzki.dagger.app.module.ActivityModule;

import java.util.Arrays;
import java.util.List;

/**
 * @author Krzysztof Kosobudzki
 */
public class BaseActivity extends Activity {
    private ObjectGraph mActivityObjectGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App app = (App) getApplication();
        mActivityObjectGraph = app.getObjectGraph().plus(getModules().toArray());

        mActivityObjectGraph.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mActivityObjectGraph = null;
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new ActivityModule(this));
    }
}
